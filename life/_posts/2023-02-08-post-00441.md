---
layout: post
title: "전략적 포지셔닝 - 최명기"
toc: true
---

 병원경영에서 바이블이라고 할 "병원이 경영을 만나다"...의 저자분이 쓴 글을 옮긴다..

 최근에.. 쁘띠성형, 피부에서 액취증, 여유증 등의 수술로 전환을 크게 하고 있는 시점에서
 다시한번 곱씹는다..

 최명기(DUKE 단과대학 의료경영 MBA / 경희대학교 의료경영학과 겸임교수)
 병원이나 기업의 CEO들이 경영의 어려움을 토로하면서 나한테 좋은 경영전략을 알려달라고 하고는 한다.

 인터넷을 검색하거나 서점에 가보면 전략의 홍수다. “블루오션 전략”, “경쟁전략”, “마케팅 전략”, “혁신 전략”, “원가경영” 등등 헤아릴 생령 없이 많은 전략에 대한 자료와 책들이 있다. 다만 좋은 전략이란 무엇일까? 우선 설마한들 좋은 전략이더라도 기미 몸에 맞아야 한다. 그쪽 전략을 시행할 배짱, 리더십, 자금, 조직이 있어야 하고 자기 전략이 우리 진료소 문화에 맞아야 한다. 좋은 전략이 있냐는 CEO들의 질문은 좋은 약을 써 달라는 환자들의 막연한 요구와 일맥상통한다. 좋은 약과 나쁜 약이 있는 것이 아니다. 자기 환자에게 효과가 있고, 부작용이 없으면서, 가급적 가격이 낮으면 그것이 좋은 약이다. 재차 나왔다고 과연 좋은 약이고, 비싸다고 어쨌든지 좋은 약이 아니다. 전략도 그와 마찬가지다.
 더더군다나 전략적 경영은 많은 위험을 감수해야 한다. 병원의 금전적, 인적 자원을 집중적으로 투자해야 한다. 그렇기 그러니까 실패를 하게 되면 병원이 도산할 수도 있다. 곧 위험을 즐기는 사람에게는 전략적 경영이 맞지만, 위험을 두려워하는 사람들에게는 전략적 사업 자체가 맞지 않는다. 위험을 두려워하는 이들이 남의 말만 믿고 위험하지만 성공했을 형편 보상이 큰 전략을 도입했다가, 사소한 어려움에 압도되어서 중도하차 하는 경우도 심심치 않게 본다. 이도 저도 아닌 것이다. 도리어 만약에 전략적 경영을 염두에 두고자 하는 분들을 위해서 한국에서의 병영 경영 전략에 대해서 지금 살펴보고자 한다.
 회사 전략의 군장 마이클 포터는 남들이 기이 하고 있는 방식대로 개연 한층 잘하는 것은 운영효율성을 증가시키는 것이라고 정의한다. 높은 운영효율성은 남들과 차별화된 실행력과 학습조직을 갖추지 않는 한 언젠가는 따라 잡히게 된다. 더구나 계속적으로 운영효율성을 높게 추구하다가 보면 언젠가는 고객들의 기대를 over-shooting하게 된다. 그러다가 시장이 급변하면 도태된다. 고로 경쟁자들과는 다른 것을 추구하거나 아니면 같은 것이라도 남들과 다른 방식으로 추구하는 것을 마이클 포터는 전략적 포지셔닝이라고 정의한다.
 예를 들어서 1980년대, 1990년대, 그리고 2000년대 초반까지 일반외과, 정형외과, 신경외과 등 외과계 개원의의 최고의 직무 모델은 물리치료 위주의 외래와 교통사고 위주의 입원병동을 아울러 운영하는 것이었다. 외래에서는 의료 시간이 길지 않는 물리치료만 받고자 하는 단순한 통증 환자를 가급적 무진히 봐서 이익을 극대화 한다. 입원에서는 영업력이 큰 사무장을 통해 교통사고 환자를 섭외해서 병상 가동률을 극대화한다. 이런즉 보편적 사무 모델에서 벗어나서 수술전문병원이라는 전략적 포지셔닝을 추구한 이들도 있었는데, 그대 대표적인 모델이 대장항문 전문병원인 송도병원, 척추수술 전문병원 우리들병원, 관절수술 전문병원 힘찬병원 등이다. 다 외과 견련 전문의들은 지금 외래 물리치료, 입원 교통사고 장사 모델의 수익률도 글로 나쁘지 않고 웅예 연관된 의료사고에 대한 두려움 그리하여 수술을 전문으로 하는 병원을 기피했다. 그러나 교통사고가 감소해 입원환자가 줄고, 입원환자의 외출, 외박, 입원 기간에 대한 자가용 보험사의 간섭도 심해지고 있다. 재활의학, 마취과 전문의들이 통증치료에 적극적으로 진출하면서 외래도 경쟁이 커졌다. 그러면서 이제는 오히려 수술을 전문으로 하는 병원이 수익이라는 점에 있어 가일층 매력적인 사업 모델이 되었다.
 그렇기는 해도 좋은 전략이라는 것은 남들이 대응하기가 쉽지 않아야 한다. 남들이 모방하기가 용이한 업무 모델은 경쟁이 격화되면서 노형 수익률이 급속히 떨어지게 된다. 따라서 수술전문병원이라는 모델에 대해서 초기에 경쟁자들이 대응하기 어려웠던 이유를 언젠가 살펴보자.
 경쟁자들은 기존의 경영 모델을 청산하고 처음부터 거듭 시도하던가, 아니면 양다리 걸치기를 하게 되는데 종전 전략적 포지셔닝을 도시 재구축한 예를 살펴보자. 안세 병원은 본시 논현동에 위치한 준 종합병원이었다. 강남에 병원들이 많지 않았던 때 일찌감치 진출해서 내외산소 위주로 보험환자 진료를 했다. 그럼에도 불구하고 외래 환자의 세월 압구정동, 신사동에 의원들이 수두룩이 개원하면서 경쟁이 심해졌다. 입원환자도 클리닉 인근의 한복판 상위 계층 환자들이 서울아산병원이나 삼성병원을 선택하면서 새로운 전략을 모색했어야 한다. 그런 시점에서 기존의 내외산소 위주의 처무 모델을 버리고, 척추 수술에 경험이 많은 의료인을 유치해서 척추전문병원으로 과감히 전환을 시도했다. 턴어라운드(Turn-around)의 결과는 성공적이었다.

 (철우의 주석: 결국에는 실패했다.. 나의 생각으로는 의료진의 문제, 시스템의 화천 문제..)

 이렇게 전략적 포지셔닝을 재구축해서 성공한 예도 있지만, 포지셔닝 재구축은 자칫 잘못하게 되면 큰 손해만 보고 낭패하는 수로 있다. 타이밍을 놓치게 되면 벌써 굉장히 많은 경쟁자들이 뛰어든 후가 된다. 보험 위주의 의원을 하다가 매각하고 뒤늦게 비만 가료 위주 비급여 의원을 열었다가 실패한 이들도 적지 않았다. 시장이 생각지도 않게 바뀔 중간 있다. 대학병원들이 다들 경쟁적으로 암병동을 신축하고 있지만 조기암검진이 보편화 되어서 위암, 대장암, 유방암, 자궁암 등이 조기 발견되어서 보험급여가 가능한 수술로 대부분 대다수 치료되게 된다면, 비급여 암 치료를 노리고 증축되는 암 병동의 경영은 심각한 국면을 맞이하게 된다.
 오히려 대다수가 행하는 대응전략은 양다리 걸치기다. 현재의 고객도 잃지 않으면서 새로운 고객도 잡겠다는 것인데 생각보다 쉽지 않다. 교통사고환자를 대략 받던 정형외과 보건소 중에 척추병원으로 전환을 시도한 경우가 있었다. 병동의 절반은 기존대로 교통사고 환자를 보고, 병동의 절반은 척추병동으로 바꾸었다. 바꿔 말하면 고객들은 이익 병원의 척추수술에 대해서 크게 신뢰하지 않았다. 교통사고 환자를 속박 받고 있었기 그렇게 교통사고 전문병원이라는 이미지가 바뀌지 않은 것이다. 교통사고 환부 중에는 통증만 조절하면서 병원에 입원해 있는 이들이 많았다. 이러한 교통환자들을 대하던 직원들의 태도가 수술환자를 대한다고 해서 적극적으로 한 순간에 바뀌어지기는 쉽지 않다. 한쪽에서는 수술을 앞둔 환자와 가족들이 불안하게 있는데, 한쪽에서는 교통사고 가해자, 피해자, 보험사 직원들이 목청을 높여 실랑이를 했다. 수술환자들이 입원 수속을 하는데, 교통사고 엄관 가족들은 보상도 나오지 않았는데 병원이 퇴원을 종용한다면서 원무과에서 소리지른다. 이어서 양다리 걸치기의 결과로 영리 병원이 척추전문병원으로 자리잡기까지는 많은 시간이 걸렸다. 나중에 척추병원으로 자리잡은 후에 경영진은 처음에 교통사고 시인 입원을 너무나 접었다면 더한층 어서 처소 잡았을 텐데 하면서 시행착오에 대해 후회했다. 기존의 고객도 잡고, 새로운 고객도 잡고자 하는 양다리 걸치기가 성공하기는 쉽지 않다.

 최명기(DUKE 대학당 의료경영 MBA / 경희대학교 의료경영학과 겸임교수)
 그렇다면 전략적 포지셔닝을 강구하는 것만이 최선의 선택일까? 좋은 전략이더라도 누구는 성공하고 누구는 성공하지 못하는데 거기 원인은 무엇일까? 마침내 좋은 전략에 못지않게 중요하다고 대두되는 것이 강력한 실행력과 지속적인 학습조직이다.

 아무리 좋은 전략이더라도 실행력이 약하면 소용이 없다. 그렇게 참신할 것이 없는 전략이더라도 실행력이 강력한 이가 연방 밀어붙이면 효과가 있다. 특히나 의료기관의 비진사정 수술을 굉장히 경계 사람이 잘하게 되어 있고, 환자를 엄청 대해본 이들이 예후가 내권 좋은 환자를 제외할 행우 있는 위기관리 능력을 지니게 되어 있다. 이러한 경험곡선의 실감 그렇게 강력히 근면히 집중하는 이들이 나중에는 성과를 거두게 된다. 재테크에 복리의 법칙이 있듯이 화주 로열티에도 시간의 법칙이 적용된다. 제한 자리에서 근면히 친절하게 환자를 대하다가 보면 어느 시점부터 환자들이 계속 다른 환자를 소개시켜주면서 홀연히 환자가 늘게 된다.

 네놈 다음으로 중요하게 여겨지는 것이 조직의 학습능력이다. 십중팔구 전문병원들이 처음부터 미래를 예측하고 전략을 꾸민 것은 아니다. 이승 성공한 비급여 피부과 네트워크도 1990년대 난생처음 개원했을 때는 여드름 치료, 점 제거 등 당시에 대개 오던 고객들을 주치료 대상으로 고려했다. 하지만 피부관리를 위해 레이저 시술을 받고자 하는 이들, 제모환자, 탈모증 환부 등이 범위 명씩 늘어날 때마다 관심을 기울였다. 보다 많은 비용을 기꺼이 지불하는 고객이 아무개 이들인지 살펴보면서 지금의 비급여 피부과 네트워크를 만든 것이다.

 아무 피부과 의사가 1997년 봄에 개원을 했다. 그는 소득양극화에 따른 상위 소득자의 소비증가, 자녀가 한쪽 또는 둘인 주부들의 여가시간 증가 등에 기초해 병원발전 10개년 계획을 세웠다. 전략적 경영을 하절 위해서 엄청난 자본을 투자했다. 뿐만 아니라 1997년 문의 외환위기로 인해서 엄청난 곤란을 겪게 되었다. 이어 아무도 미래를 예측할 호운 없다. 성공한 피부과 네트워크들 중에는 대외적으로 치밀한 계획과 예측에 기초한 전략적 경영으로 성공했다고 주장하는 이들이 많다. 반면 실제로는 남보다 빠르게 성장한 피부과 네트워크는 위기 병원을 찾는 고객들의 성향 변화를 남보다 기민하게 경영에 반영했을 따름이다. 바로 부근지 환경의 변화를 학습해서 경영에 반영하는 능력이 탁월할 케이스 계속적으로 기존 고객을 만족시키면서 성장할 운명 있다. 성장이 가능한 고객층을 놓치지 않고 경영에 반영을 할 핵 있는 능력, 이녁 고객을 만족시킬 새로울 방법을 파악하는 능력이 학습능력인 것이다. 이빨 학습능력이 강한 실행력에 의해서 원조 된다면 현재의 시장이 변화하지 않는 제한 강자의 지위를 유지할 목숨 있다.
 오히려 문제는 시장이 변화한다는 점이다. 1970년대까지만 해도 병의원에 대한 환자들의 perception은 계급구조적이었다. 의원보다는 종합병원이 낫고, 종합병원 중에서는 클수록 좋고, 개인종합병원보다는 대학종합병원이 낫고, 대학종합병원 중에서도 서울에 있는 병원이 낫다는 생각을 가지고 있다. 서울에 있는 클리닉 사이 어디가 낫냐는 순서는 대학교 커트라인에 의해서 정해졌다. 서울대학교 병원을 한국에서 남편 좋은 병원이라고 인식했다. 다만 1990년대 들어서 소득수준이 향상되고 기수 주관에 의해서 정보를 판별할 무망지복 있는 중산층이 늘어났다. 이제는 과거와 같은 획일적인 계급구조에 의해서 병원의 순서를 매기지 않게 되었다. 환자들의 perceptual map이 바뀐 것이다. 더더군다나 환자들은 병원에서 인격적으로 친절한 대우를 받지 못하면 참을 고갱이 없게 되었다. 이렇게 사회가 바뀌다가 보니까 병원에 대한 인식도 바뀌게 되었다. 이렇게 고객이 바뀌다가 보니까 서울아산병원과 삼성병원이 최고의 보건소 중간 하나로 인식되었다. 대학병원보다도 전문 상표 병원이 더욱더 수술을 똑바로 한다고 생각하게 되었다. 누구도 예상할 생목숨 없었던 환자들의 대풍 변화였다.
 동시에 한국경제가 성장하고 국가자산이 증가하면서 감정 테두리 사람이 끌어올 길운 있는 금융자본도 비약적으로 커지게 되었다. 그러면서 개인도 충분한 자금을 끌어들여서 좋은 인테리어와 좋은 의료기기를 갖출 명 있게 되었다. 개인 방사선과 의원도 MRI를 갖출 운 있게 되었고, 일사인 안과의원도 고가의 레이저 안과 수술도구를 리스할 복운 있게 되었다. 그렇다면 거의거의 준종합 중소병원들은 어찌어찌 이러한 시장의 변화에 근본적으로 기민하게 대처하지 못했던 것일까? 이러한 변화는 여태 생각하면 더없이 당연한 일이지만 만날 매일로 따지면 대변 더디 진행되기에 외려 속수무책이었던 것이다. 아무도 미래를 예상할 운명 없기 때문인 것이다. 미래를 예상한다고 해도 거주인구는 줄어들고, 교통은 혼잡한 곳에서 낡은 건물로 무엇을 할 성명 있었을까? 전문병원을 표방하고 건물 리노베이션에 엄청난 자본을 투입한다고 해도 입지의 불리함을 극복할 요체 있었을까? 이것 저것 하면 할수록 한층 손해만 볼 가운데 있다. 시고로 경우는 전략적 집중을 염절 보다는 적절한 시기에 병원을 그만두는 것이 [korea male breast](https://jollyagonizing.com/life/post-00037.html) 최선의 방법일 수도 있다. 즉 능동적인 대처가 주야장천 최선의 대처는 아닌 것이다. 일층 가 투자하지 않고 최소한도의 이윤만 추구하다가 그나마 우극 결과 이윤이 나오지 않으면 사업을 중지하는 것이 최선의 선택일 수행 있다. 아무것도 내군 하는 것이 최선인 경우도 있는 것이다.
 따라서 미래와 시장의 움직임에 확신을 가지고 극단적인 전략을 추구하는 것은 상천 실패의 위험을 안고 있다. 댁네 계한 예가 남성 전문 비만, 피부관리, 성형 클리닉이다. 피부관리 전문 네트워크 의원, 척추 전문 수술병원, 관절 수술 전문병원, 비만 전문 보건소 등 차별화 광풍이 불던 때에 차별화된 데서 더한층 차별화해야 한다는 생각을 가진 이들이 생겼다. 마케팅 전략상 잘못된 것은 아니었다. 선두 주자의 도리 마케팅만 잘하면 환자 수로 진료소 하나를 운영하기에는 충분했을 복판 있다. 반면에 탈모와 같은 경우를 제외하면 처음부터 남성전문 미용 클리닉을 전략적으로 집중해서 개원한 이들 반도 성공한 이들은 많지 않다. 그렇지만 기존 비만, 피부관리, 성형외과의원에 내원하는 남성환자는 늘어가고 있다. 비뇨기과 위주의 남성전문 클리닉에서 피부관리도 계통 병행하는 경우도 늘어나고 있다. 그래서 비만, 피부관리, 성형수술을 원하는 남성환자가 늘어나는 것은 틀림 없는 사실이다. 그렇지만 남성전문 클리닉은 아직까지 위치 잡지 못하고 있다. 어째서 그런지에 대해서 이거 저것 가설을 만들 수는 있다. 그럼에도 그것은 가설일 뿐이다. 나중에 남성전문 보건소 중간 크게 성공하는 곳이 생기게 되면 인제 사리 우리는 정형 이유를 알게 될 것이다. 오히려 하나의 전문 클리닉이 성공하기까지는 수 가난히 많은 이들이 남성환자를 요컨대 보는 피부, 비만 클리닉이라는 전략을 시도하다가 실패했다는 것을 잊지 말아야 한다.

 송도병원, 우리들병원, 힘찬병원, 고운세상피부과는 전략적 경영을 해서 성공한 이들이다. 반대로 자기 더더욱 몇 배 더욱 많은 이들이 성공모델이 된 의료기관보다 다과 앞서서, 내지 비슷한 시기에 비슷한 전략으로 올인 했다가 실패했다는 것을 잊지 말아야 한다. 성공했을 호시기 이녁 이익이 큰 전략일수록, 그리고 군 전략에 집중할수록, 실패했을 사례 받는 타격도 큰 것이다. 이것을 딜로이트 컨설팅의 마이클 레이머는 전략 패러독스라고 정의했다.
 처음부터 좋은 전략과 나쁜 전략이 있는 것이 아니다. 나에게 맞는 전략과 나에게 맞지 않는 전략이 있을 뿐이다. 아울러 전략적 사업 자체가 맞는 사람이 있고 맞지 않는 사람이 있다. 전략적 비즈니스 못지않게 중요한 것이 강력한 실행능력과 끊임없는 학업 능력이다. 때로는 대응하기 보다는 기다리면서 시장에서 철수할 적절한 시기를 고려해야 할 경우도 있다. 성공했을 밥 네년 보상이 큰 획기적인 전략일수록 실패했을 걸음 지불해야 하는 대가도 크다. 비록 노력해도 미래를 원체 예측할 수는 없다. 우리는 겸허하게 성공의 상당부분은 운이라는 것을 받아들여야 할지도 모른다. 그러한 운이 계속될 수도 있고 아니면 이때 멈출 수련 있다.

