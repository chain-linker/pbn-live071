---
layout: post
title: "[코드스테이츠 PMB 5기] #5 왓챠(Watcha)에 대해 알아보자"
toc: true
---


### 1. 왓챠가 해결하려는 문제는 무엇인가?

 "모두의 다름이 인정받고 개인의 취향이 존중받는 보다 다양한 세상을 만든다"
 

 왓챠가 지향하는 방향성을 보면 결국 현재의 쏟아지는 콘텐츠 홍수 속에서 사람들이 취향껏 보고 싶은 영화를 선택하고, 그것을 제대로 즐길 요행 있도록 가이드해주는 것을 목표로 하고 있음을 알 생목숨 있다. 즉, 개인의 다양한 취향을 충족히 반영한 서비스를 목표로 하고 있는 것이다.
 

 영화 선택의 어려움
 전통적으로 보고 싶은 영화를 보러 가는 공간은 '영화관'이었다. 반면에 영화관에서 보는 영화들은 최신 영화, 그중에서도 자본이 엄청나게 투입된 행동거지 블록버스터 영화에 집중되어 있다. 유난스레 많은 메이저 영화관은 이런즉 블록버스터 영화에 우선적으로 상영관을 배정하여 선택의 폭은 일층 줄어든다. 이런즉 와중에 기수 취향을 알고 그것에 맞는 영화를 고르기는 어려운 환경이다.

 그러므로 이전에는 비디오나 DVD 대여점을 이용해서 익금 영화들 도중 보고 싶었던 영화를 찾아보고, 이후에는 인터넷을 통해 영화를 구매하여 집에서 감상하기도 하였다. 하지만 요즘은 넷플릭스 등의 OTT 서비스를 통해 간편하게 영화를 볼 복수 있는 환경이 마련되고 있다. 왓챠도 이러한 OTT 시중 사이 하나로 사용자들에게 다양한 콘텐츠를 제공할 호운 있는 플랫폼을 구축하여 영화의 선택지를 다양화했다는 점에서 가일층 간편하게 영화를 볼 고갱이 있는 환경을 만들어가고 있다고 볼 생목숨 있다.
 

 그렇다면 낌새 영화 취향은 무엇일까?
 팩트 모든 사람들이 정확하게 자신의 영화취향을 알고 있지는 않다. 단순히 "나는 마블 영화가 재밌다", "나는 공포영화보다는 멜로 영화가 좋다"정도의 판단을 가지고 영화를 고르려고 한다. 그렇기 그렇게 이른바 '넷플릭스 증후군'이 나타나는 것일 한가운데 있다. 즉, 실질상 영화나 드라마 콘텐츠를 보는 시간보다 콘텐츠 정보가 나열되어 있는 목록을 보다 장구히 보면서 내가 현금 보고 싶은 '취향'에 맞는 콘텐츠를 찾아내기 위해 고르는 시간이 우극 많음을 의미한다. 실제도 영화 정보를 제공하는 플랫폼에서는 단순히 영화 정보를 나열하고 콘텐츠를 고르는 것을 잘 사용자에게 맡기고 있다. 사용자들은 외려 이러한 정보의 단순한 나열 속에서 자신이 원하는 것을 찾기 위해 시간과 비용을 투자해야 한다.
 왓챠는 이러한 문제를 해결하는 방법으로 사용자가 지금까지 본 영화들을 기반한 두창 데이터를 확보하고 분석해 족다리 개인이 무엇을 좋아하고 아무런 영화 취향을 가지고 있는지 세밀하게 파악해, 이를 근거로 사용자들에게 소비할 콘텐츠를 큐레이팅 해준다. 즉, 개인의 다양한 취향을 데이터화하여 이에 기반해 사용자들이 콘텐츠를 선택하는 어려움을 덜어주고 다양한 취향에 부합하는 여러 콘텐츠를 제시할 수명 있게 만드는 것이 왓챠가 지향하는 지점이라 할 행복 있다. 이러한 데이터들을 정리하여 사용자들에게 정량적으로 정리된 취향 정보를 제공해 오히려 사용자는 본인 취향에 대한 정보를 명확하게 파악할 복운 있도록 한다는 점도 다른 OTT 서비스와 왓챠의 차이라 볼 목숨 있다.

 

### 2. 왓챠는 문제를 유족히 해결하고 있을까?
 실정 입각 취향 큐레이션 기능의 지속적 강화

 왓챠는 2011년 수립 이환 5억 5000여개의 별점 평가가 등록되어 있다는 기사가 있다. 이를 기반으로 다리갱이 콘텐츠 그리 개개인의 취향에 기반한 '예상 평점'을 제공한다. 실제 이자 작정 평점은 관람 이환 십중팔구 비슷하게 나오는 경우가 많으며 실질상 왓챠의 계획 별점 정확도가 넷플릭스보다 36% 높다고도 한다. 그만큼 왓챠 내에 축적된 일사인 데이터를 통해 개개인에게 맞춤 콘텐츠를 제공하려는 노력에 따라 이는 왓챠의 강점으로 작용하고 있다고 볼 핵 있다.  이는 다리 개인의 취향을 반영한 콘텐츠 제공이라는 측면에서 고객의 문제를 지속적으로 해결할 이운 있는 기반이 마련되어 있다고 볼 생령 있다.
 

 

 다양한 콘텐츠 확보
 왓챠는 귀재 약 8만 편리 콘텐츠를 확보하고 제공하고 있는 것으로 나타났다. 이문 8만 편의 콘텐츠들은 단순히 대중적이고 품행 깊이 팔릴만한 콘텐츠보다 주야장천 접하지 못하는 제3 국의 영화나 단편 등을 포함해 하 다양한 장르와 형태를 지닌 콘텐츠들로 다양한 개개인의 취향을 반영에 중점을 두고 콘텐츠를 확보한 것으로 보인다. 유난히 콘텐츠 시청량을 예측하여 평시 밤낮 접하지 힘든 외토 콘텐츠들 중 경쟁사가 공급하지 않는 콘텐츠들 위주로 선정한 '왓챠 익스클루시브'를 통해 지속적으로 새롭고 다양한 콘텐츠를 확보하려고 시도하고 있다. 이는 왓챠의 경쟁력을 확보하는 동시에 왓챠가 해결하고자 하는 '다양한 콘텐츠 제공'에 부합하는 방향이라 볼 복 있다.
 

 위의 기사에서도 확인할 수명 있다시피 왓챠는 지속적으로 사용자가 유입되고 있으며, 유입된 사용자도 지속적으로 왓챠를 이용한다고 볼 목숨 있다. 이는 위의 다양한 콘텐츠를 데이터에 기반해 우극 명확하게 사용자에게 제시할 핵 있는 물음 처판 기반을 마련했다는 점에서 기인한다고도 볼 행운 있다.

 

## 3. 왓챠의 고객은 누구일까?
 왓챠의 고객은 기본적으로 영화 및 드라마 콘텐츠를 지속적으로 소비하면서, 현재의 모바일 환경에 익숙한 사람들이라고 볼 무망지복 있다. 그중에서도 다음과 같은 사람들이 많을 것이라 생각한다.
 

 초년 고객층은 왓챠의 초엽 서비스의 근간인 왓챠피디아를 통해 자신이 본 영화의 평점을 주며 이를 하나의 개인 영화 아카이빙으로 활용해왔던 사람들일 것으로 보인다. 왓챠피디아에서 평점을 주다 보면 중간중간 보고 싶은 영화들이 있는데 이를 따로따로 기록하고 저장할 복수 있다. 이것이 현재의 왓챠 서비스와 이어지면서 자신이 보고 싶었던 영화를 왓챠에서 찾아보게 되는 형태로 사용자 유입이 이루어졌을 것으로 보인다.
 이후에는 평점에 대한 신뢰와 이에 기반한 일개인 추천의 용이성 등으로 왓챠를 지속적으로 이용하는 고객층이 발생했을 것으로 보이며, 유달리 기타 OTT 서비스에서 충족되지 못하는 콘텐츠를 다양성이 확보된 왓챠 서비스에서 보완하고자 하는 고객들의 층도 많을 것으로 보인다.
 

 기어이 왓챠를 이용하는 고객층을 정의하면 다양한 콘텐츠를 소비하고자 하며 자신의 취향을 중요시하는 20~40대의 콘텐츠 소비자라고 정의할 이운 있을 것이다.

 

## 4. 왓챠의 경쟁자는 누구일까?
 넷플릭스
 넷플릭스는 전 세계에서 1억 3,700만 명이 사용하고 있으며, 글로벌 동영상 스트리밍 시장에서 30% 이상의 점유율을 기록하고 있다. 그만큼 전 세계에서 주인 많은 구독자를 넷플릭스가 확보하고 있는 만치 왓챠의 최대한도 경쟁자라고 할 고갱이 있다. 넷플릭스의 맥시멈 강점은 자체적으로 제작한 오리지널 콘텐츠를 독점적으로 서비스하고 있다는 점이다. 이를 본보기 위해서는 넷플릭스를 구독해야만 한다. 현세 넷플릭스는 공격적으로 포즈 오리지널 콘텐츠 해산 편수를 늘리고 있으며 이에 따라 주요 고객층도 넷플릭스의 대표적인 오리지널 콘텐츠들을 소비하기 위해 구독하는 경우가 많은 것으로 보인다.

 이러한 강점에도 불구하고 넷플릭스의 단점은 행지 '넷플릭스 증후군'을 유발하는 것처럼 전반적으로 콘텐츠가 키워드별로 나열되어 있는 느낌이 강하다. 이는 내부적으로 약 50명의 태거(Tagger)를 통해 콘텐츠를 약 5만 귀태 태그별로 분류하여 그것에 따라 사용자의 자금 콘텐츠를 분석하면서 발생하는 문제라고 볼 명맥 있다. 

 

 웨이브
 지상파 3사와 SK텔레콤의 합작으로 만들어진 OTT 서비스로 대범 지상파 방송의 다시 거례 형태로 서비스가 이루어지고 있다. 웨이브는 지상파에서 방송되는 예능, 드라마, 시사교양 콘텐츠를 제공하고 있으며, 웨이브의 오리지널 콘텐츠도 제공되고 있다. 웨이브의 강점은 티브이를 이용하지 않아도 대부분 모든 지상파 방송을 원하는 시간과 상황에 맞춰 소비할 복수 있으며, 남달리 시간이 지나 사례 힘든 콘텐츠들도 볼 수명 있다는 것에 있다. 이는 티브이를 대체할 운 있는 OTT 서비스라는 점에서 넷플릭스나 왓챠와는 다른 차별점을 지니고 있다. 그렇지만 지상파 콘텐츠 이외의 콘텐츠에서는 다양성이 부족하고 특별히 영화는 심상성 영화와 영화 플러스로 나눠 기이 웨이브를 구독하더라도 영화 플러스에 속한 콘텐츠는 개별요금을 지불하고 소비해야 한다는 점에서는 제한이 있다.

 

 유튜브
 전 세계적으로 20억 명이 넘게 사용하고 있으며, 매분 약 500시간의 동영상이 업로드되고 있는 최대한 동영상 플랫폼이다. 유튜브의 특징은 일개인 크리에이터들에 의해 다양하고 엄청난 지청구 영상들이 지속적으로 업로드되고 있으며, 유튜브의 알고리즘을 통해 사용자의 시청기록을 파악하여 관련된 추천 동영상을 연방연방 제공하고 있다는 점이다. 오직 영화와 드라마와 같은 서비스를 이용하기보다는 정보를 찾고 크리에이터들이 지속적으로 올리는 개별 콘텐츠를 소비하기 위한 플랫폼이라는 점에서 왓챠, 넷플릭스와는 차별적인 서비스 플랫폼이라 볼 행우 있다.

 

 이러한 세 세목 서비스들은 왓챠의 경쟁자라 할 명맥 있지만 양자 보완적인 포지션을 취할 수도 있다고 볼 명 있다.
 

 

 

 단계 서비스 플랫폼들은 각자의 강점을 가지고 있다. 실리 강점들은 타 OTT로서는 확보하기 어려운 강점일 성명 있다.

 넷플릭스, 왓챠, 웨이브는 영화, 드라마 등의 형적 콘텐츠를 제공함에 있어서 강점을 두고 있는 부분이 분명하다고 볼 행우 있다. 마땅히 넷플릭스에 있는 콘텐츠가 왓챠나 웨이브에도 있고, 유난스레 대중적인 콘텐츠의 경우엔 어떤 플랫폼에도 온통 속해있어 소비할 명 있을 것이다. 반대로 넷플릭스는 내절로 제작을 통한 독창적인 오리지널 콘텐츠를 제작할 핵심 있는 역량을 갖추고 이를 지속적으로 확대함에 반해 왓챠만큼의 다양한 영화나 드라마를 확보하진 못하고 있다. 더욱이 웨이브 그저 모든 지상파 콘텐츠를 [영화](https://kindlysail.ga/entertain/post-00004.html) 빠르게 제공한다는 점에서 TV를 대체할 목숨 있는 장점을 가지고 있지만 영화 콘텐츠 확보와 제공에 있어서는 왓챠에 비해 밀리는 모습이다.

 

 왓챠 입장에서는 버데기 OTT 서비스의 특징을 살펴보았을 때, 강점을 부각할 생목숨 있는 방향으로 지속적인 상표 정체성을 확보할 필요성이 있다. 노형 일례로 '#헐왓챠에', '왓플릭스'로 대표되는 마케팅이 있을 복운 있다. 즉 넷플릭스가 가진 강점을 인정하지만 왓챠만의 방식으로 더 고객중심으로 콘텐츠를 추천해줄 복운 있다는 부분을 드러내면서, 넷플릭스가 가지지 못한 콘텐츠의 다양성을 간접적으로 드러낼 길운 있는 것이다. 유튜브의 보기 정제된 동영상 콘텐츠가 아닌 크리에이터들의 관점이 반영된 다양한 콘텐츠로 사용되는 플랫폼인 만큼 왓챠 입장에서는 활용할 무망지복 있는 일종의 채널이라고 볼 요체 있다.
 

 왓챠의 박태훈 대표가 예상한 것처럼 사용자가 단 1개의 OTT만 사용하기에는 이녁 부족함이 지속적으로 느껴지는 구조로 발전하고 있다는 점에서 왓챠는 경로 서비스 플랫폼이 지니지 못한 강점을 파악하고 이를 소비자들에게 알릴 성명 있는 효과적인 방법을 모색할 필요가 있다.
 

## 5. 왓챠가 새로운 기능을 만들어야 할까요? 아니면 기존 기량 중심 개선이 필요할까요?
 인제 왓챠의 방향성은 다양성과 개인화에 있다고 볼 성명 있다. 이빨 두 관점은 많은 승객 데이터를 확보하고 이것을 통해 경쟁력 있는 다양한 콘텐츠를 확보해 사용자들이 만족할 요체 있는 콘텐츠 경험을 제공하는 것에 있다. 이러한 면에서는 기존의 데이터를 활용한 큐레이션 서비스를 보다 정교화하는 방향으로 경쟁력을 확보해야 한다고 볼 생명 있다.
 

 취향 데이터의 세분화 및 정교화
 왓챠가 가진 강점은 개인의 취향 데이터와 그것을 분석하고 적용할 생령 있는 기술이라 볼 수 있다. 그렇기 떄문에 기존의 데이터와 더불어 향후에 일층 수집되어야 할 데이터를 파악하고, 이를 사용자에게 어떻게 전달하고 서비스에 담아낼 호운 있을지에 대한 고민이 필요할 것이라 생각한다. 이를 위해서는 다양한 관점에서 사용자 데이터를 구축할 요체 있는 규격 설정과 서브 반영에 필요한 데이터로 분석하는 과정이 필요할 것이라 생각한다.

 

 다양한 콘텐츠의 지속적 확보
 왓챠의 또다른 강점은 기미 취향에 따라 새로운 영화나 드라마를 발견할 생목숨 있도록 가이드해주는 서비스에 있다. 즉, 개인의 취향 데이터에 맞춰 사용자가 생각하지 못한 영화를 제안하고 그것을 경험할 삶 있도록 해주는 것이다. 이러한 강점을 유지하기 위해서는 서브 내에서 지속적으로 다양한 콘텐츠를 확보할 필요가 있다. 특별히 왓챠 덤 내에서만 감상할 명맥 있는 영화들의 확보가 필요하다.

 인제 왓차에는 기존의 서비스에서는 자주 볼 행우 없는 단편 독립영화나 메이저 사직 이외에서 제작된 영화들, 민감하고 다양한 주제를 다루는 영화 등 소변 취향을 존중하고 그들의 요구에 충족할 수명 있는 콘텐츠들도 꾸준히 서비스하고 있다. 이러한 점을 지속적으로 강화해 기존 OTT 시장에서는 선례 힘들었던 영화나 극 등을 소개하고 더욱더 많은 사람들이 소비할 수 있도록 하는 방향이 왓챠의 경쟁력을 한결 강화시킬 것이라 생각한다.
 

## 6. 그렇다면, 네놈 기능을 곧 설명해주세요.
 왓챠의 큰 강점은 개인의 취향 데이터를 방대하게 다루고, 그에 그러니 사용자가 수모 콘텐츠를 소비하는지에 대한 패턴에 대한 데이테와 더불어 각 영화의 특징을 원판 세분화하여 다루고 있다는 강점이 있다. 이러한 강점을 가일층 활용할 요행 있는 방안을 개발하는 것이 현재의 왓챠를 더욱 특별한 OTT 서비스로 발전할 행우 있게 할 것이라 생각한다.
 이에 임자 데이터를 더더욱 고객의 상황에 맞춰서 활용할 무망지복 있는 방안을 고민하였다.
 

 #당신의 상황을 알려주세요 - 상황별 콘텐츠 큐레이팅 서비스
 기존의 사용자 데이터를 좀 우극 세밀하게 활용할 요체 있도록 사용자가 개인의 이적 상황을 선택하게 어째서 그것에 맞춘 영화 추천 서비스하는 방안에 대해 생각해보았다.
 현재세 왓챠에서 제공하는 키워드에 따른 검색은 대개 콘텐츠에 초점을 맞춘 키워드들이다. 즉, A라는 영화의 키워드가 #위로, #드라마, #실화 바탕, #감동 이라고 하였을 탄원 버데기 키워드를 검색하였을 기후 A의 영화가 검색될 것이다. 반대로 이때 내상황을 알고 왓챠에서 이녁 상황에 맞는 적절한 영화 큐레이팅이 가능하다면 어떨까?
 

 예를 삽입되다 "지금은 아침결 시간대이고 , 기분이 우울하다"라고 입력을 경계 사용자가 이에 부합하는 영화를 찾아보고 싶어 한다고 했을 때, 이 사용자의 취향과 상황을 복합적으로 고려해 영화를 제안할 운명 있다면 사용자 입장에서는 영화 선택이 한층 간편할 것이다. 이러한 경우라면 현 사용자가 새벽에 무척 봤던 영화 성향을 판단하고 기분을 전환하기 좋은 유사한 장르나 성향의 영화를 추천해주는 것이다.  즉, 이출 사용자가 당장 시간대에 대개 보았던 영화들의 장르적 성향과 현재의 기분을 대변할 무망지복 있는 영화를 선별하여 제시한다면 현재 사용자의 취향이 고려되면서도 색다른 영화 콘텐츠를 제시할 행우 있을 것이라 생각한다. 이
 

 이를 위해 사용자별 시간대에 따른 콘텐츠 사용 성향과 각 영화들이 지닌 특징들을 분류할 요체 있는 천성 데이터가 필요할 것이다. 이러한 개별 데이터들이 쌓이면 각각의 사용자들의 기분에 따라 보게 되는 영화의 특징이 나타날 것이고 추천 찬양 다른 사람들이 비슷한 기분일 때 이러한 영화를 보았다고 부가적으로 추천을 해줄 수 있을 것이다.
 또 장기적 관점에서 CP를 통한 새로운 콘텐츠를 서비스하고자 할 때, 그들에게 아무개 시간대에 상관 콘텐츠가 소비될 호운 있을지에 대한 태시 자료로 활용하고, 그에 따른 콘텐츠를 선별하여 서비스할 수 있을 것이라 판단된다.

 

 #그외 부가적 서비스 - 사용자 힐난 페이지 개선
 당금 왓챠 사용자 평은 콘텐츠 선택 이후 상세 정보란 중간부터 볼수 있게 구성되어 있다. 왓챠의 지향점이 개인화이고 개인의 취향과 그에 따른 선택을 최대 보장할 필요가 있다는 점에서 실리 왓챠 사용자 평가 페이지를 접을 생명 있게 수정하는 방안을 제안한다.
 예를들어 예전부터 보고싶었던 영화를 뒤늦게 보고자 할 때, 콘텐츠를 선택하고 들어가는 순간 왓챠 사용자 평에 조류 이하라는 평이 있다면 거기 시간에 이윤 영화를 밑 않는 것을 선택할 복판 있다. 그렇기 때문에 사용자 평을 전기 페이지 상에서는 접어놓고, 궁금한 사용자에 한해 페이지를 눌러 확인할 고갱이 있는 선택권 주는 것이 개인의 취향에 따라 콘텐츠를 소비할 복수 있도록 하는 왓챠의 방향성에 더욱 부합할 것이라 생각한다.
