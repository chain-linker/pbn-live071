---
layout: post
title: "[해외축구 프리뷰] 지상 최대의 축구 전쟁, 엘클라시코"
toc: true
---

안녕하세요 ♪
 주말이 시작됐습니다!
 금번 주말에 스페인에서 참말 큰 경기가 있죠~
 즉속히 엘클라시코라고 불리는
 바르셀로나와 레알마드리드의 경기입니다!
 석자 호날두가 소속되어있을 때의

 극한의 긴장감은 아니지만
 그래도 지구촌 최대의 라이벌이 펼치는 축구 전쟁을

 이참 시간에 언제 프리뷰 해보겠습니다.
 아시겠지만, 저는 여러 팀을 좋아하고 축구 경기 시청을 즐기는
 평범한 블로거 이므로
 어느 팀의 특정 팬이 아님을 알려드리고
 활기차게 시작하겠습니다!
 

 

 

 ▷  스포츠 날짜  :  2020년 10월 24일 토요일  <한국 시간>
 ▷  대결 시간  :  밤 11시  <한국 시간>
 ▷  운동경기 장소  :  캄프 누  <바르셀로나 홈 구장>
 ▶  바르셀로나  :  9위  /  4경기 - 2승 1무 1패  /  8득점 2실점  /  승점 7점
 ▶  레알마드리드  :  3위  /  5경기 - 3승 1무 1패  /  6득점 3실점  /  승점 10점
 

 

 ★  주목할 점  ★
 서로를 넘어야 왕좌를 차지하는 두 팀이 만났습니다.
 이제는 라이벌을 넘어 인생살이 경쟁이라고 평가받는 엘클라시코입니다.
 두 팀은 전 세계가 주목하는 거조 빅클럽입니다.
 최근에는 조용히 주춤하고 있지만
 스페인을 넘어 전 세계에서 100여 년을 이어온 라이벌입니다.
 그야말로 지독한 라이벌이라고 해야 할 거 같습니다.
 스페인 라리가 역대 전적은 180경기 , 레알 73승 , 바르셀로나 72승 , 무승부 35 입니다.
 투 경기로 계산하면 244경기 , 레알 96승 , 바르셀로나 96승 , 무승부 52 입니다.
  정말 억지로 짜고 하려도 할 명 없는 팽팽한 전적입니다.
 금차 시즌 첫 번째 엘클라시코를 통해 누가 우위를 가져갈지

 사항 궁금하게 만드는 어떤 뜨락 시합 역사 것입니다!
 

 

 

 ◈  멀리보기 라인업  ◈
 

 

 ◈  부상 및 결장자 명단  ◈
 테어 슈터겐 / 사무엘 움티티
 

 ◈  요마적 이슈  ◈
 - 시즌 전 현계 파동을 겪었던 메시는 오히려 에이스입니다.
 하지만 이적 리그 4경기에서 단 1[스포츠](https://imgcompression.com/sports/post-00001.html)골 만을 기록했습니다.
 뿐만 아니라 엘클라시코 900일, 440분 거리 목적 침묵을 하고 있습니다.
 - 테어 슈터겐이 물팍 부상으로 출전이 힘든 가운데

 티키타카로 대변되는 빌드업이 어려움을 겪을 것으로 예상됩니다.
 - 저번 라운드에서 헤타페에게 1대0 패배를 겪었습니다.
 - 체사 파티, 쿠티뉴 등 컨디션이 좋은 선수들에게 기대를 합니다.
 

 ◈  플레이 스타일  ◈
 이번 해 시즌 다시 부임한 쿠만 감독의 지휘 아래
 전통적인 4-3-3 포메이션을 버리고 4-2-3-1 포메이션으로 변경했습니다.
 중원은 부스케츠와 데 용이 위치해 수비진을 단단하게 보호하며
 공격진으로 연결고리를 이어나가 있습니다.
 공격진에는 어린 나이의 눈석임물 파티와 돌아온 쿠티뉴가 거취 중입니다.
 이금 둘의 활약으로 메시가 결정을 짓는 모습을 보일 것입니다.
 과거의 티키타카로 대변되는 완벽한 패스워크, 점유율 장악의 모습은
 목하 상당히 볼 행복 없지만, 도리어 점유와 실리를 챙기는

 바르셀로나의 강한 플레이를 기대하고 있습니다.
 

 ◈  우려되는 부분  ◈
 1. 테어 슈터겐 결장
 테어 슈터겐의 무릎 부상으로 네토 서브 키퍼가 출전할 예상입니다.
 바르셀로나의 밖주인 큰 걱정이라고 생각합니다.
 선방과 빌드업 등 일평생 최고의 골키퍼가

 출전하지 못한다면, 이전 부분에서 문제가 될 것입니다.
 2. 오른쪽 윙 파괴력
 지금 오른쪽 윙 자리는
 그리즈만과 뎀벨레가 번갈아 맡고 있습니다.
 그럼에도 불구하고 시즌 초반에는 왼쪽 윙 유체 파티에 비교해서
 활약이 많지 않습니다.
 3. 주전들의 체력
 시즌 초반이라 크게 나타나지는 않을 것입니다.
 그렇지만 피케, 알바, 부스케츠, 메시 등은
 30대가 넘은 나이로

 후반에는 체력적인 문제를 겪을 것입니다.
 

 

 

 ▣  조망 라인업  ▣
 

 

 ▣  부상 및 결장자 명단  ▣
 카르바할 / 오르디오 솔라 / 아자르 / 외데고르
 

 ▣  간경 이슈  ▣
 - 만근 벤제마의 표목 결정력이 줄었습니다.
 마땅히 연계나 잉여 다른 모습에서 팀에 기여하지만
 아자르도 부상, 부진으로 힘겨운 지금
 벤제마의 골이 터져야 할 것입니다.
 - 라모스의 부상으로 수비진이 흔들렸지만
 금번 경기에는 복귀하여 수비진을 지휘할 것입니다.
 - 그렇기는 해도 카르바할에 부상은 파트 공격의 위력이

 워낙 줄어들 것으로 예상됩니다.
 - 부군 큰 이슈는 리그와 챔스, 2경기 내리 패배했다는 점입니다.
 3경기 영속 패배가 대개 없는 레알마드리드는
 이번 경기에서 중원을 강화하여 원정에서의 패배를

 얻지 않으려고 노력할 것입니다.
 

 ▣  플레이 스타일  ▣
 이참 게임 전에 리그에서는 승격팀 카디스 0-1 패배,
 챔스에서는 샤흐타르에 2-3 패배를 겪으며
 팀 가스 자체가 가라앉아 있습니다.
 그러면 요번 경기에서는 중원을 단단히 하여

 원호 않으려는 전략을 들고 나올 것입니다.
 4-3-1-2 포메이션을 통해 중원을 활용하여
 경기를 풀어나갈 것입니다.
 빠르고 파괴력이 강한 윙어들이 많은 레알이지만
 이번 시즌에는 전체적으로 그편 퍼포먼스가
 좋지 않으므로 중원 강화 전략을 펼칠 것입니다.
 

 ▣  우려되는 부분  ▣
 1. 벤제마의 낮아진 퍼포먼스
 저번 시즌 21골을 기록했던 벤제마는
 이참 시즌 5경기 1골을 기록하고 있습니다.
 전체적인 팀 경기력이 낮아진 상태에서
 벤제마의 표적 결정력이 나오지 않는다면
 레알은 이번 경기도 쉽지 않을 것입니다.
 2. 카르바할의 부재
 오른쪽의 수비와 공격을 십중팔구 책임지고 있는
 카르바할은 무르팍 부상으로
 이참 경기뿐만 아니라, 2개월 체장 출전할 행우 없습니다.
 이출 포지션에는 나초나 경미한 부상으로 오드리오 솔라가
 출전할 핵심 있겠지만. 수비력과 공격력 면에서

 카르바할이 그리워질 것입니다.
 3. 부진한 공격진
 벤제마와 비니시우스가 레알의 공격진을 이끌고 있지만
 다른 빅클럽과 비교하면 이년 파괴력이 낮다고 볼 핵심 있습니다.
 아센시오부터 아자르, 로드리구, 요비치 등
 교체로 나오게 될 다른 공격진의 활약이 필요합니다.
 

 

 이렇게 이번에는 고장 최대의 더비 라이벌
 엘클라시코의 바르셀로나와 레알마드리드의 경기를
 언제 프리뷰 해보았습니다.
 두 팀 실총 예정의 이년 강력함은 조금 줄어들었지만,
 서로를 이겨야만 살아야 하는 관계이므로
 참것 치열할 것으로 예상됩니다.
 저도 더없이 진정 기대됩니다 ^^
 송두리 재밌게 보시길 바랍니다 ㅎㅎ
 

 

 

